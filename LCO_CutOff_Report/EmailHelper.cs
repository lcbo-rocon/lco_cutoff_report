﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace LCO_CutOff_Report
{
    public static class EmailHelper
    {
        public static bool SendHTMLMail(DataTable dt
            ,string mailServer
            ,string emailFrom
            ,string emailTo
            ,string emailbcc
            ,string subject)
        {
            bool mailOK = true;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(emailFrom);

                foreach (string email in emailTo.Split(","))
                {
                    message.To.Add(new MailAddress(email));
                }
                message.Bcc.Add(new MailAddress(emailbcc));


                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = "<p>As of " + DateTime.Now + ", there are " + dt.Rows.Count.ToString() + " customer order missed cutoff found!</p>"
                    + message.Body + DataTableToHTML.ExportDatatableToHtml(dt);

                SmtpClient client = new SmtpClient();
                client.Host = mailServer; 

                client.Send(message);


            }
            catch (Exception ex)
            {
                mailOK = false;
            }
                    //  Console.WriteLine("Sent Successfully!");

            return mailOK;
        }
       
    }
}
