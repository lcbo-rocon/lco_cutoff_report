﻿///<summary>
/// LCO_CutOff_Report V1.00
/// This program extracts list of AGY customer from ROCON that have missed their cutoff time for placing orders
/// and sends an email to recipient list outlined in App.config file
/// 
/// Created by itszl, August 2019
/// Reviewed by
/// 
/// </summary>

using System;
using System.Configuration;
using System.Data;
using System.IO;

namespace LCO_Missing_CutOff_Alert
{
    class Program
    {
        static void Main(string[] args)
        {
            string LogFileName = ConfigurationManager.AppSettings["LogFilePath"].ToString() + "\\" + ConfigurationManager.AppSettings["LogFileName"].ToString() + DateTime.Now.ToString("yyyyMMdd") + ".log";
            FileStream fileLog = File.Open(LogFileName, FileMode.Append);
            StreamWriter flog = new StreamWriter(fileLog);
            int OffsetDays = Convert.ToInt32(ConfigurationManager.AppSettings["OffsetDays"].ToString());

            AppLog.WriteLog(flog, "Program LCO_Cutoof_Report V1.00 Started");

            AppLog.WriteLog(flog, "OffsetDays - " + OffsetDays.ToString()
                + ". Looking for: " + DateTime.Today.AddDays(OffsetDays).ToString("yyyy-MM-dd"));

            AppLog.WriteLog(flog, "Retrieving missed cutoff customers");

            DataTable dt = SQLHelper.GetMissedCutoffCustomers(OffsetDays);

            if (dt.Rows.Count > 0)
            {
                AppLog.WriteLog(flog, "There are " + dt.Rows.Count + " missed. Preparing to send email...");
                string emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                string emailTo = ConfigurationManager.AppSettings[
                    ConfigurationManager.AppSettings["Environment"].ToString() + "EmailTo"].ToString();
                string emailcc = ConfigurationManager.AppSettings["EmailBCC"].ToString();
                string subject = ConfigurationManager.AppSettings["Subject"].ToString() + DateTime.Today.ToString("yyyy-MM-dd");
                string mailServer = ConfigurationManager.AppSettings["MailServer"].ToString();

                AppLog.WriteLog(flog, "Email From: " + emailFrom);
                AppLog.WriteLog(flog, "Email To: " + emailTo);
                AppLog.WriteLog(flog, "Email CC: " + emailcc);
                AppLog.WriteLog(flog, "Email Subject: " + subject);
                AppLog.WriteLog(flog, "Mail Server: " + mailServer);

                if (EmailHelper.SendHTMLMail(dt, mailServer, emailFrom, emailTo, emailcc, subject))
                {
                    AppLog.WriteLog(flog, "Email sent out successfully!");

                }
                else
                {
                    AppLog.WriteLog(flog, "Failed to send email.");
                }
            }
            else
            {
                AppLog.WriteLog(flog, "There are no missed orders today!");
            }

            AppLog.WriteLog(flog, "Program Ended at " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

            flog.Close();

        }
    }
}
