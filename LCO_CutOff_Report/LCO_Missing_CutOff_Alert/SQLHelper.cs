﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Configuration;

namespace LCO_Missing_CutOff_Alert
{
    public static class  SQLHelper
    {
        public static OracleConnection GetConnection()
        {
            //string connectionstring = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=RSG003)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=roc8.world)));User Id=lcbo;Password=lcbo";
            string env = ConfigurationManager.AppSettings["Environment"];
            string connectionstring = ConfigurationManager.AppSettings[env];
            return new OracleConnection(connectionstring);
        }

        public static OracleCommand GetDBCommand(string query)
        {
            OracleCommand DBCommand = new OracleCommand(query, GetConnection());

            return DBCommand;
        }

        public static DataTable GetMissedCutoffCustomers(int offsetDays=0)
        {
            DataTable dtCutoff = new DataTable();
            DataSet ds = new DataSet();

            string query = "LCBO.GET_LCO_MISSED_CUTOFF";
            //string query = "LCBO.GET_CUSTOMER_INFO"; 
            OracleCommand DBCommand = GetDBCommand(query);

            DBCommand.Connection.Open();
            DBCommand.CommandType = CommandType.StoredProcedure;
            DBCommand.BindByName = true;

            DBCommand.Parameters.Clear();
            DBCommand.Parameters.Add(new OracleParameter("LV_OFFSET", offsetDays));
            DBCommand.Parameters.Add(new OracleParameter("REF_REPORT", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            //DBCommand.Parameters.Add(new OracleParameter("LV_CUSTOMER", "933333"));
            //DBCommand.Parameters.Add(new OracleParameter("REF_CUSTOMER", OracleDbType.RefCursor)).Direction= ParameterDirection.Output;

            //dtCutoff.Load(DBCommand.ExecuteReader());
            OracleDataAdapter adap = new OracleDataAdapter();
            adap.SelectCommand = DBCommand;
            adap.Fill(ds);

            dtCutoff = ds.Tables[0];
            DBCommand.Connection.Close();

        
            return dtCutoff;
        }
    }
}
