﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LCO_Missing_CutOff_Alert
{
    public static class AppLog
    {
        public static void WriteLog(StreamWriter fw, string msg)
        {
            try
            {

                fw.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + ": " + msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing to Log file!" + ex.Message.ToString());
            }

        }
    }
}
